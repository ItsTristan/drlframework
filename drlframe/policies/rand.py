#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

"""

"""
import base

import random

class RandomPolicy(base.BasePolicy):
    def choose(self, actions, value_fn, training=False):
        """
        Choose an action from the list
        :actions: a list of ints corresponding to actions
        :value_fn: function that maps from action to expected value
        :training: Set to True to use the training epsilon
        """
        return random.choice(actions)
