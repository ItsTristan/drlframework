#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

"""

"""
from abc import ABCMeta, abstractmethod

class BasePolicy:
    __metaclass__ = ABCMeta

    @abstractmethod
    def choose(self, state, actions, value_fn, training=False):
        """
        Choose an action from the list, using the provided value function
        :actions: a list of ints corresponding to actions
        :value_fn: function that maps from action to expected value
        :training: Set to True to use the training epsilon
        """
        return actions[0]

    def end_episode(self):
        """
        Signals to the policy that the episode has ended, so any
        post episode updates can happen
        """
        pass

    def save(self, fname):
        """
        Saves the policy to a file
        :fname: Name of the file to save to
        """
        raise NotImplementedError()

    def load(self, fname):
        """
        Loads the policy from a file
        :fname: Name of the file to save to
        """
        raise NotImplementedError()
