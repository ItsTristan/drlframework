#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

"""

"""
import base

import random

class EpsilonGreedy(base.BasePolicy):
    def __init__(self, epsilon=0.95, test_epsilon=0.05, decay=0.99, decay_freq=1000):
        """
        :epsilon: Exploration rate
        :test_epsilon: Epsilon to use if the not in training mode
        :decay: Multiplier for epsilon to decay gradually
        :decay_freq: Number of episodes to wait before applying the decay
        """
        self.epsilon = epsilon
        self._train_eps = epsilon
        self._test_eps = test_epsilon
        self.decay = decay
        self.decay_freq = decay_freq
        self._ep_count = 0

    def choose(self, actions, value_fn, training=False):
        """
        Choose an action from the list
        :actions: a list of ints corresponding to actions
        :value_fn: function that maps from action to expected value
        :training: Set to True to use the training epsilon
        """
        eps = [self._test_eps, self.epsilon][training]

        if random.random() < eps:
            return random.choice(actions)
        else:
            return max(actions, key=value_fn)

    def end_episode(self):
        """
        Signals the end of an episode
        """
        self._ep_count += 1
        if self._ep_count % self.decay_freq == 0:
            self.epsilon *= self.decay
