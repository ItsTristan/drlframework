#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

"""
TicTacToe using images from the X perspective
"""

import numpy as np
import Image, ImageDraw
import matplotlib.pyplot as plt

import base

import random

class TicTacToe(base.BaseEnvironment):
    def __init__(self, im_scale=10, opp='random'):
        """Create the board object

        :im_scale: The relative scale of images compared to the base board [10]
        """
        self._im_scale = im_scale

        if opp == 'random':
            self._opp = RandomPlayer()
        elif opp == 'human':
            self._opp = HumanPlayer()
        else:
            raise ValueError('Unknown opponent type %s' % opp)

        self.reset()

    def state(self):
        """
        :returns: the current state of the environment, as a numpy array
        """
        return self._imstate

    def actions(self):
        """
        :returns: the actions available to the agent as a list of ints
        """
        return range(9)

    def act(self, action):
        """
        Applies an action to the environment and returns a reward.
        Rewards are:
             0 if no event
            +1 if X wins
             0 for tie
            -1 if X loses
            -1 for invalid
        :action: The action to take
        :returns: The reward for taking that action
        """
        a = divmod(action, 3)
        # If space is taken, kill the game
        if self._state[a] != 0:
            self._image = None
            return -1

        self._state[a] = self._player
        if self.check_win():
            # If game is won, kill the game
            self._image = None
            if self._player == 1:
                return 1
            else:
                return -1
        # Check draw
        self._turns_left -= 1
        if self._turns_left == 0:
            self._image = None
            return 0
        self._player = -self._player
        if self._player == -1:
            assert self._turns_left > 1
            assert self._turns_left % 2 == 0
            reward = self.act(self._opp.get(self.get_valid_actions()))
            if reward != 0:
                self._image = None
            assert sum(self._state.flatten()) == 0., '\n' + repr(self)
            return reward
        else:
            assert self._turns_left > 0
            assert self._turns_left % 2 == 1
        self.update_image()
        return 0

    def update_image(self):
        if self._image is None:
            return
        D = ImageDraw.Draw(self._image)
        for i in xrange(1,3):
            D.line([i*self._im_scale, 0, i*self._im_scale, 3*self._im_scale])
            D.line([0, i*self._im_scale, 3*self._im_scale, i*self._im_scale])
        for i in xrange(3):
            for j in xrange(3):
                pos = int(j*self._im_scale+2), int(i*self._im_scale)
                if self._state[i,j] == 1:
                    D.text(pos, 'X')
                elif self._state[i,j] == -1:
                    D.text(pos, 'O')
        del D
        self._imstate = np.asarray(self._image, dtype='float32')
        self._imstate = self._imstate[:,:,0] / 255.

    def get_valid_actions(self):
        return filter(lambda i: self._state[divmod(i,3)] == 0, range(9))

    def check_win(self):
        # Check columns
        T = self._state
        if np.any(np.all(T == self._player, 0)):
            return True
        if np.any(np.all(T == self._player, 1)):
            return True
        if T[1,1] == self._player:
            if T[0,0] == T[1,1] == T[2,2]:
                return True
            if T[2,0] == T[1,1] == T[0,2]:
                return True
        return False

    def reset(self):
        """
        Resets the environment to initial conditions
        """
        self._state = np.zeros((3, 3))
        self._image = Image.new("RGB", (self._im_scale*3, self._im_scale*3))
        self.update_image()
        self._player = 1
        self._turns_left = 9

    def state_shape(self):
        """
        Returns the shape of the environment state space
        :returns: A tuple of the size of the state np array
        """
        return 3*self._im_scale, 3*self._im_scale

    def num_actions(self):
        """
        :returns: the number of unique actions
        """
        return 9

    def is_terminated(self):
        """
        :returns: False if the environment has reached a terminus
        """
        return self._image is None

    def display(self):
        """
        Displays the environment
        """
        print self

    def __repr__(self):
        c = [' ','X','O']
        return '{}|{}|{}\n-+-+-\n{}|{}|{}\n-+-+-\n{}|{}|{}'.format(*list(map(lambda x: c[int(x)], self._state.flatten().tolist())))

class RandomPlayer:
    def get(self, available):
        return random.choice(available)

class HumanPlayer:
    def get(self, available):
        while True:
            try:
                row = raw_input('row> ')
                col = raw_input('col> ')
                act = row*3+col
                if act not in available:
                    raise ValueError()
                break
            except ValueError:
                print "Invalid input."
        return act

