#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

from abc import ABCMeta, abstractmethod

class BaseEnvironment:
    """
    The Parent environment for other environments.
    Subclass this to ensure compatibility with the rest
    of the framework
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def state(self):
        """
        :returns: the current state of the environment, as a numpy array
        """
        import numpy as np
        return

    @abstractmethod
    def actions(self):
        """
        :returns: the actions available to the agent as a list of ints
        """
        return []

    @abstractmethod
    def act(self, action):
        """
        Applies an action to the environment and returns a reward
        :action: The action to take
        :returns: The reward for taking that action
        """
        return 0

    @abstractmethod
    def reset(self):
        """
        Resets the environment to initial conditions
        """
        pass

    @abstractmethod
    def state_shape(self):
        """
        Returns the shape of the environment state space
        :returns: A tuple of the size of the state np array
        """
        return ()

    @abstractmethod
    def num_actions(self):
        """
        :returns: the number of unique actions
        """
        return 0

    @abstractmethod
    def is_terminated(self):
        """
        :returns: False if the environment has reached a terminus
        """
        return False

    def display(self):
        """
        Displays the environment
        """
        print self
