#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

"""
Non-realtime snake game for testing RL
"""

import numpy as np
from numpy import random
from collections import deque

import base

_EMPTY = 0
_PLAYER = 1
_BODY = 2
_FOOD = 3

_LEFT = 0
_RIGHT = 1
_UP = 2
_DOWN = 3

class Snake(base.BaseEnvironment):
    """Snake game testing bed for RL"""

    def __init__(self, width=10, height=10):
        assert width >= 3 and height >= 3, "Board is too small"
        self._width = width
        self._height = height
        self.reset()

    def state(self):
        """
        :returns: the current state of the environment, as a numpy array
        """
        if self._dead:
            return None
        return self._board

    def actions(self):
        """
        :returns: the actions available to the agent as a list of ints
        """
        return (_LEFT, _RIGHT, _UP, _DOWN)

    def act(self, action):
        """
        Applies an action to the environment and returns a reward
        :action: The action to take
        :returns: The reward for taking that action
        """
        # Don't allow the player to move backwards
        if action == self._direction^1:
            action = self._direction

        # Check where the player is gonna land
        reward = 0
        x,y = self.peek_move(action)
        if self.is_outside(x,y):
            self._dead = True
            return 0
        elif self[x,y] == _BODY:
            self._dead = True
            return 0
        elif self[x,y] == _FOOD:
            reward = 1
            self._length += 1
            self._move_food()

        # Move the player
        self._board[self._y, self._x] = _BODY
        self._board[y,x] = _PLAYER
        self._tail.append( (self._x, self._y) )
        self._x,self._y = x,y
        self._direction = action
        if len(self._tail) > self._length:
            x,y = self._tail.popleft()
            self._board[y,x] = _EMPTY

        return reward

    def reset(self):
        """
        Resets the environment to initial conditions
        """
        self._board = np.zeros((self._height, self._width))
        self._direction = random.randint(4)
        self._length = 4

        self._y = random.randint(1,self._height-1)
        self._x = random.randint(1,self._width-1)
        self._board[self._y, self._x] = _PLAYER

        self._tail = deque()
        self._tail.append( (self._x, self._y) )

        self._move_food()

        self._dead = False

    def state_shape(self):
        """
        Returns the shape of the environment state space
        :returns: A tuple of the size of the state np array
        """
        return (self._width, self._height)

    def num_actions(self):
        """
        :returns: the number of unique actions
        """
        return 4

    def is_terminated(self):
        """
        :returns: False if the environment has reached a terminus
        """
        if self._dead:
            return True
        return False

    def display(self):
        """
        Displays the environment
        """
        print self

    ### END OF ENVIRONMENT ###


    def is_outside(self,x,y):
        """
        :returns: True if the given coordinate is not on the board
        """
        return not (0 <= x < self._width and 0 <= y < self._height)

    def __getitem__(self, key):
        x,y = key
        return self._board[y,x]

    def peek_move(self, direction):
        """
        Look at the move without taking that action
        :returns: the tile that would be landed on when the player moves
        """
        xmove = [-1,1,0,0][direction]
        ymove = [0,0,-1,1][direction]
        return self._x+xmove, self._y+ymove

    def _move_food(self):
        """
        Randomly move the food
        """
        self._foody = random.randint(self._height)
        self._foodx = random.randint(self._width)
        while True:
            if self[self._foodx, self._foody] == _EMPTY:
                break
            self._foody = random.randint(self._height)
            self._foodx = random.randint(self._width)
        self._board[self._foody, self._foodx] = _FOOD

    def __repr__(self):
        """
        Prints the board.
        O = head
        o = body
        + = food
        . = wall
        """
        s = '.' + '.' * self._width
        for row in xrange(self._height):
            s += '.\n.'
            for col in xrange(self._width):
                s += [' ','O','o','+'][int(self._board[row,col])]
        return s + '.\n..' + '.'*self._width

def game():
    board = Snake()
    score = 1

    while True:
        state,actions = board.state()

        if state is None:
            break

        print board
        print 'L: %d, R: %d, U: %d, D: %d' % (_LEFT,_RIGHT,_UP,_DOWN)
        d = int(raw_input('>>> '))

        score += board.act(d)

    print "Game over. Score = %d" % score

if __name__ == '__main__':
    game()
