#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

import base
import numpy as np
from collections import deque
import random

class KerasQN(base.BaseQModel):
    def __init__(self, nn, gamma, exp_count=10000, replay_freq=1, memory_limit=16384, mini_batch_size=32):
        """
        :estimator: The q-value estimator.
        :nn: The neural network to train
        """
        self._nn = nn
        self.mini_batch_size = mini_batch_size

        self._input_shape = list(nn.input_shape[1:])
        self._output_shape = list(nn.output_shape[1:])

        self._memory_limit = memory_limit
        self._memory = deque(maxlen=memory_limit)
        self._exp_count = exp_count
        self.replay_freq = replay_freq

        self.gamma = gamma

    def value(self, state, action):
        """
        Returns the value of the action in the given state.
        :state: The state to evaluate, as an np array
        :action: The action to evaluate under that state, as an int
        """
        values = self._nn.predict(state.reshape([1]+self._input_shape), batch_size=1)
        return values[0,action]

    def value_fn(self, state):
        values = self._nn.predict(state.reshape([1]+self._input_shape), batch_size=1)
        def fn(act):
            return values[0,act]
        return fn

    def update(self, state, action, reward, next_state):
        self._memory.append( (state, action, reward, next_state) )

    def end_episode(self):
        """
        Learn using experience replay
        """

        n_exp = self.replay_freq*self.mini_batch_size
        if len(self._memory) < max(self._exp_count, n_exp):
            return
        if self.replay_freq <= 0:
            return

        X_train = []
        Y_train = []
        for batch in xrange(self.replay_freq):
            mini_batch = random.sample(self._memory, self.mini_batch_size)

            input_shape = list(self._input_shape)
            output_shape = list(self._output_shape)

            for exp in mini_batch:
                S, A, R, Sp = exp
                if Sp is not None:
                    Q, Qp = self._nn.predict(np.array([S,Sp]).reshape([2]+input_shape), batch_size=2)
                else:
                    Q = self._nn.predict(S.reshape([1]+input_shape), batch_size=1)
                    Qp = np.zeros([1])

                S = S.reshape(input_shape)
                Q = Q.flatten()
                Q[A] = R + self.gamma*Qp.max()
                X_train.append(S)
                Y_train.append(Q)

        self._nn.fit(
                np.array(X_train),
                np.array(Y_train),
                batch_size=self.mini_batch_size,
                nb_epoch=1, verbose=0)

