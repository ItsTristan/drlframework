#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

import base
from collections import defaultdict
from numpy import random

class MemoCoder(base.BaseQModel):
    def __init__(self, inputshape, noutputs, estimator,
            init=lambda: 0.01*random.randn()):
        """
        :inputshape: The shape of the input, as a tuple of ints
        :noutputs: The number of outputs (actions) the model is expected to learn
        :estimator: The q-value estimator.
        :init: The initialization function. Default: random ~ N(0, 0.01)
        """
        base.BaseQModel.__init__(self, inputshape, noutputs, estimator)
        self._memory = defaultdict(init)

    def value(self, state, action):
        """
        Returns the value of the action in the given state.
        :state: The state to evaluate, as an np array
        :action: The action to evaluate under that state, as an int
        """
        return self._memory[self._key(state), action]

    def _key(self, state):
        return tuple(state.flatten())

    def _learn(self, batch):
        """
        Learns from a batch update. This is where the actual learning happens.
        :batch: A list of tuples of the form (state, action, exp_return)
            where exp_return is the expected return
        """
        for state,action,exp in batch:
            self._memory[state,action] += exp - self._memory[state,action]
