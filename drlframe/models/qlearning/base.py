#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

class BaseQModel:
    """
    Representation of Q-values for the RL agent
    """
    def value(self, state, action):
        """
        Returns the value of the action in the given state.
        :state: The state to evaluate, as an np array
        :action: The action to evaluate under that state, as an int
        """
        raise NotImplementedError('Subclass must override value')

    def value_fn(self, state):
        """
        Returns a function that maps from actions to state values.
        :state: The state to evaluate, as an np array
        """
        def fn(action):
            return self.value(state, action)
        return fn

    def update(self, state, action, reward, next_state):
        """
        Updates the internal model representation given the
        upate parameters. Specifically, this fetches batches from
        the estimator model and learns from them, rather than
        learning from the result directly.

        The purpose of this split is so that different function
        estimators can be combined with different updating
        schemes. For instance, DQL can use Experience Replay
        or Limited Eligibility traces.

        :state: The state to being updated, as an np array
        :action: The action to update under that state, as an int
        :reward: The received reward for taking that action under that state
        :next_state: The state entered for taking the action
        """
        raise NotImplementedError('Subclass must override update')

    def end_episode(self):
        """
        Signal to the qlearner that the episode has ended
        """
        pass

    def save(self, fname):
        pass

    def load(self, fname):
        pass

