#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

class BasicAgent:
    """
    A basic agent object. Has a model and a policy.
    Any other agents should extend BasicAgent for
    compatibility, but this is not enforced.
    """
    def __init__(self, model, policy):
        self.model = model
        self.policy = policy

    def train(self, env, epochs=10000, verbose=1, print_freq=1000):
        """
        Trains the agent on the given environment.

        :env: The environment object to train on
        :epochs: Number of episodes to run for
        :verbose: Print the mean return periodically (default 1)
        :print_freq: Number of episodes before printing the mean return
        :returns: the mean return after training
        """
        mean_return = 0.
        for epoch in xrange(1, epochs+1):
            ep_return = self._run_episode(env, True)
            mean_return += (ep_return - mean_return) / min(epoch, print_freq)

            if epoch % print_freq == 0:
                print "Epoch {}, mean return = {}".format(epoch, mean_return)

        return mean_return

    def test(self, env, epochs=10000, display=False, verbose=1, print_freq=1000):
        """
        Runs the agent on the given environment without any training

        :env: The environment object to train on
        :epochs: Number of episodes to run for
        :display: Run the environments display function (use for demoing)
        :verbose: print the mean return periodically
        :print_freq: Number of episodes before printing the mean return
        :returns: the mean return after testing
        """
        mean_return = 0.
        for epoch in xrange(1, epochs+1):
            ep_return = self._run_episode(env, False, display=display)
            mean_return += (ep_return - mean_return) / min(epoch, print_freq)
            if epoch % print_freq == 0:
                print "Epoch {}, mean return = {}".format(epoch, mean_return)
        return mean_return

    def run(self, env):
        """
        Runs the environment once, displaying every time.
        """
        return self.test(env, 1, True, False)

    def _run_episode(self, env, training, display=False):
        """
        Runs a single episode and returns the episode return
        [Internal function]

        :env: The environment to run on
        :training: whether to use training mode or not
        :display: whether or not to call the environment's display function
        """
        ep_return = 0.
        env.reset()

        state = env.state()
        while not env.is_terminated():
            if display:
                env.display()
            action, value = self.eval_state(state, env.actions(), training=training)
            reward = env.act(action)
            ep_return += reward

            new_state = env.state()
            self.model.update(state,action,reward,new_state)
            state = new_state

        self.policy.end_episode()
        self.model.end_episode()

        return ep_return

    def get_action(self, env, training=False):
        """
        Gets an action according to the policy
        :env: The environment
        :returns: An action under the given state, or None if already terminal
        """
        state = env.state()
        if state is None:
            return None
        actions = env.actions()
        value_fn = self.model.value_fn(state)
        self.policy.choose(state,actions,value_fn,training=training)

    def eval(self, env, training=False):
        """
        Examines the state of env and returns an action
        and an expected return for that action
        :env: The environment to examine
        :training: Whether to use the training policy or not
        :returns: an action and its expected return
        """
        return self.eval_state(env.state(), env.actions(), training=training)

    def eval_state(self, state, actions, training=False):
        """
        Returns an action and an expected return
        given the state and a list of actions
        :state: The np array representing the environment state
        :actions: A list of ints representing actions of the environment
        :training: Whether or not to use the training policy
        :returns: An action and its expected return
        """
        value_fn = self.model.value_fn(state)
        action = self.policy.choose(actions, value_fn, training=training)
        return action, value_fn(action)

    def save(self, fname):
        """
        Saves the model to disk
        :fname: The name of the file to save
        """
        raise NotImplementedError('')

    def load(self):
        """
        Loads the model from disk
        :fname: The name of the file to load
        """
        raise NotImplementedError('')
