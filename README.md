This package is a deep q-learning framework, with the intention of
being highly modular.

## Setup

To setup the project, it is recommended that you use a virtual environment.
Then, you can use:
        setup.py develop

to setup the project in the environment

## Examples

You should be able to run the samples using:
        python samples/<example_file.py>


