#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

"""

"""
from drlframe.agents import BasicAgent
from drlframe.models.qlearning import KerasQN
from drlframe.models.estimators import QLearning, EligibilityTraces
from drlframe.policies import EpsilonGreedy
from drlframe.environments import Snake

from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers import Convolution2D, Flatten, MaxPooling2D
from keras.optimizers import RMSprop

def main():
    env = Snake()
#    estimator = ExperienceReplay(10000, env.state_shape())
#    estimator = QLearning(0.95)
    estimator = EligibilityTraces(0.95, 0.9)
    nn = init_model(env.state_shape(), env.num_actions())
    model = KerasQN(estimator, nn)
    policy = EpsilonGreedy(0.95, 0.05, 0.99, 1000)
    agent = BasicAgent(model, policy)

    print "Mean return (training): %.04f" % agent.train(env)
    print "Mean return (testing): %.04f" % agent.test(env)

    agent.run(env)

def init_model(ninputs, noutputs):
    ninputs = [1] + list(ninputs)

    nn = Sequential()
    nn.add(Flatten(input_shape=ninputs))
    nn.add(Dense(256))
    nn.add(Activation('relu'))
    nn.add(Dropout(0.5))
    nn.add(Dense(noutputs))
    nn.add(Activation('linear'))
    nn.compile(loss='mse', optimizer=RMSprop())
    return nn

if __name__ == "__main__":
    main()

