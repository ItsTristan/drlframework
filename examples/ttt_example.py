#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

"""

"""
from drlframe.agents import BasicAgent
from drlframe.models.qlearning import KerasQN
from drlframe.policies import EpsilonGreedy
from drlframe.environments import Snake, TicTacToe

from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers import Convolution2D, Flatten, MaxPooling2D
from keras.optimizers import RMSprop

def main():
#    env = Snake()
    env = TicTacToe()
    print env
    nn = init_model(env.state_shape(), env.num_actions())
    model = KerasQN(nn, 0.9, replay_freq=1)
    policy = EpsilonGreedy(1., 0.05, 0.9705, 1000)
    agent = BasicAgent(model, policy)

    print "Starting training..."
    try:
        print "Mean return (training): %.04f" % agent.train(env, 100000, print_freq=100)
    except KeyboardInterrupt:
        pass
    model.replay_freq = 0 # Fully disable learning
    print "Starting testing..."
    print "Mean return (testing): %.04f" % agent.test(env, 10000, print_freq=100)

    agent.run(env)

def init_model(ninputs, noutputs):
    nn = Sequential()
    if len(ninputs) == 2:
        ninputs = [1]+list(ninputs)

    nn.add(Convolution2D(32, 3, 3,
        border_mode='valid',
        input_shape=ninputs))
    nn.add(Activation('relu'))
    nn.add(Convolution2D(32, 3, 3))
    nn.add(Activation('relu'))
    nn.add(MaxPooling2D(pool_size=(2,2)))
    nn.add(Dropout(0.25))

    nn.add(Flatten())
    nn.add(Dense(128))
    nn.add(Activation('relu'))
    nn.add(Dropout(0.5))
    nn.add(Dense(noutputs))
    nn.add(Activation('linear'))
    nn.compile(loss='mse', optimizer=RMSprop())
    return nn

if __name__ == "__main__":
    main()

