#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshko@ualberta.ca>
#
# Distributed under terms of the MIT license.

"""

"""
from setuptools import setup

setup(name='drl_frame',
        version='0.1',
        description='Deep Q-learning framework',
        url='bitbucket.org/ItsTristan/qrl_frame',
        author='Tristan Meleshko',
        license='MIT',
        packages=['drlframe'],
        install_requires=['numpy','scipy','keras'],
        )

